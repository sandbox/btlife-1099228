
Description:
------------
This module extends the webform module to easily allow submitting a webform 
to Salesforce.com.  It utilizes the Salesforce API Toolkit, which must be downloaded
separately from the module.  In order to use the Salesforce API Toolkit, your
Salesforce installation must be either the Enterprise, Ultimate, or Developer
editions.

The Salesforce API Toolkit can be downloaded freely from
http://wiki.apexdevnet.com/index.php/Web_Services_API#PHP
under the 'Toolkit' listing.  Make sure you follow the instructions on the
project page or the INSTALL.TXT file very closely to make sure the API is 
installed correctly.

The Salesforce Webform module does require PHP with SOAP enabled.  The 2.x
branch of the Salesforce Webform module only supports Webform's 3.x branch.
If you are looking to use Salesforce Webform with the Webform's 2.x branch,
please download and install Salesforce Webform's 1.x version.

If you are having issues with blank screens or errors on form submissions,
as a first troubleshooting step, make sure you have entered the correct
username and password + Security Token combination on the administration
page.

This module was sponsored by by Davis Applied Technology College, 
http://www.datc.edu.  The module was programmed by Obsidian Design LLP,
http://www.obsidiandesign.com.

The module is based, in part, on the sugarwebform module.
