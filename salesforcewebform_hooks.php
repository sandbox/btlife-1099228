<?php

/**
 * Implement salesforcewebform_get_salesforce_objects hook
 * This hook allows a special case to be added to the options drop-down
 * @param $options
 *  List of all available Salesforce Objects
 *
 *  @return
 *    array of options to add to the supported list
 **/
function sfw_contact_account_get_salesforce_objects($options) {
  // check if object code key is already taken, this will happen
  // if two modules are using the same object code key
  if(isset($options['m'])) {
    drupal_set_message(t('Salesforce Object Code m already in use.'), 'error');
  }
  $items = array();
  // if both Contact and Account are active then enable Account + Contact type
  if(isset($options['a']) && isset($options['c']) ) {
    $items['m'] = t('Account + Contact');
  } else {
    drupal_set_message(t('Salesforce Object Account or Contact is not enabled in Salesforce API, Account + Contact option removed.'),'warning');
  }
  return $items;
}

/**
 * Implement salesforcewebform_get_sf_names
 * This hook allows the special case to add it's required salesforce objects to the list
 *
 *  @param $sf_object_code
 *    A single char code representing this special case
 *
 *  @return
 *    array of Salesforce Object names to add that this module uses
 *
 **/
function sfw_contact_account_get_sf_names($sf_object_code) {
  if($sf_object_code == 'm') {
    return array('Account','Contact');
  } else {
    return array();
  }
}

/**
 * Implement salesforcewebform_submit_to_salesforce
 * This hook allows for specific processing of this type
 * Primarily used to provide the order in which to submit the records to
 * Salesforce, and the transfer of reference keys, (i.e. submit record 1, use the Id
 * returned to populate a field on record 2, then submit record 2)
 *
 *  @param $sf_object_code
 *    The single char code representing this special case
 *
 *  @param $all_fields
 *    An array of key/value pairs of the user submitted data
 *    key - form_key/Salesforce API Name
 *    value - user or form provided(hidden fields) value
 *
 *  @return
 *    18 char Salesforce Id for the record that was created or updated
 *
 **/
function sfw_contact_account_submit_to_salesforce($sf_object_code, $all_fields) {
  if($sf_object_code == 'm') {
      $account_id = _submit_to_salesforce($all_fields, 'Account');
/** Sample check for exists, don't update if it exists
    // Check if Account record exists, if it does don't update it
    $matches = _sf_record_exists("SELECT id FROM Account WHERE name = '%s' ORDER BY LastModifiedDate", $all_fields['name']);
    $num_matches = count($matches);
    if($num_matches == 1) {
      $account_id = $matches[$num_matches-1]->id;
    } elseif(count($matches) > 1) {
      // More than one record matched, take the most recent one updated
      $account_id = $matches[$num_matches-1];
    } else
      // No matches found, create it
      $account_id = _submit_to_salesforce($all_fields, 'Account');
    }
**/
   $all_fields['accountid'] = $account_id;
    // Account has an API Field Name of 'Name' which is also in the contact but is read-only
    // Remove the name field since we are done submitting the Account record
    unset($all_fields['name']);
    $new_id = _submit_to_salesforce($all_fields, 'Contact');
  } else {
    // If this module didn't handle the request
    return FALSE;
  }
}



/**
 * Sample Exists check
 *
 * @param $query
 *    query to run against salesforce to see if the record is already there
 *
 *  This function is in the module in case a basic lookup isn't enough for an existence check
 *  A more complex example would be to right a function that does multiple queries to check 
 *  several Salesforce conditions in order to determine existence.
 *
 *  NOTE: this is not a hook, it is meant to be an example that can be added to external modules that need it
 **/

/* 
function _sf_record_exists($query) {
  // Check for existing record
    try {
      $result = $sf->client->query($query);
    }
    catch (Exception $e) {
      watchdog(SALESFORCE_LOG_SOME, 'Exception in existing record look up: ' . $e->getMessage(), array(), WATCHDOG_ALERT);
      return;
    }
    
    if($result->size > 0) {
      return $result->records;
    } else {
      return array();
    }
  }
}
*/
