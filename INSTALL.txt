Requirements
------------
Drupal 6.x, Salesforce.com account, PHP + SOAP enabled, Webform 3.x, Salesforce Suite (Salesforce API module enabled)


Upgrading
------------
Always make sure to run update.php when upgrading the module to a new version

If you are upgrading from the 1.x to the 2.x branch, check to make sure that
the field mappings are maintained.  Users moving from the 1.2->2.0 editions
may have fields that do not remained mapped due to a change in the way
Salesforce Webform handles the mappings.


Installation
------------
1. Copy the salesforcewebform directory to the Drupal modules directory for your site

2. In your Salesforce.com screen, add a new custom field 'WebformSID'.  Select that it is
   a Unique Field and an External ID.  It is very important to add this field, as it
   maps each Webform submission to an object in Salesforce, and allows edits of the Webform
   submission to not create a duplicate object; instead, the existing object is updated.

3. Create or edit a webform to match your object form.  Under 'Salesforce Settings', select
   the 'Yes' option to post the form to Salesforce.

   Know required fields will automatically be added to the Webform, and will be verified that
   they exist each time the webform is saved.  However, some Salesforce object have optionally
   required fields so you must set these up. 
   Example Event ActivityDate (quote from Salesforce API Documentation): 
    "If the Event IsAllDayEvent flag is set to true (indicating that it is an all day Event), 
     then the Event due date information is contained in the ActivityDate field. This field 
     is a date field with a timestamp that is always set to midnight in the Coordinated 
     Universal Time (UTC) time zone. The timestamp is not relevant, and you should not attempt
     to alter it to account for any time zone differences. Label is Due Date Only.  This field
     is required in version 12.0 and earlier if the IsAllDayEvent flag is set to true.
     The value for this field and StartDateTime must match, or one of them must be null."
   
   For each component, you must select a Salesforce field to map to.  The default fields
   are listed for easy mapping.  There is an Auto Create Yes/No on the form edit, if Yes then ALL
   optional fields are added to the form.  This might be overkill if your form is going to be small,
   but it does allow you to pick and choose which fields you want by removing any fields you don't need.
   Time wise it would be faster to set it to No and add the fields you want, instead of having it add
   them all, and then removing 3/4ths of the added fields.

   As the last entry in the Webform, add a hidden field 'SID Key'.  Select 'Webform SID Key'
   as the Salesforce key to map to.  It is very important that the SID Key be the LAST item in
   the form.

   Save the webform.

4. As your object form is submitted (either new or edits), it will be posted via SOAP to 
   Salesforce automatically.
